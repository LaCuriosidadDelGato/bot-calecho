#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Descargar libreria Mastodon en: https://github.com/halcy/Mastodon.py
# pip install Mastodon.py
# pip3 install Mastodon.py
from mastodon import Mastodon

from bs4 import BeautifulSoup
import sqlite3
import time

MASTO_TOKEN = '...'
MASTO_URL = 'https://botsin.space'
CUENTA_ADMIN = 'LaCuriosidadDelGato@mastodon.social'

def estado_existe(url,api):
    try:
        estado = api.search_v2(url)
        if len(estado.statuses)!=1:
            return [False,'No existe el estado']
        else:
            id = estado.statuses[0].id
            return [id,estado.statuses[0],'Existe']
    except:
        return [False,False,'No existe el estado']

def impulsar(url,api):
    '''
    estado = api.search_v2(url)
    
    if len(estado.statuses)!=1:
        return [False,'No existe el estado']

    id = estado.statuses[0].id
    '''
    id,estado,msj = estado_existe(url,api)
        
    if id:
        return [id,msj]
    try:
        status = api.status_unreblog(id)
        time.sleep(2)
    except:
        pass
    
    try:
        api.status_reblog(id)
        return [True,'Impulso hecho']
    except:
        return [False,'No se puede impulsar']
    
def lista_notis(_since_id, api):
    notis = []
    
    while True:
        try:
            _notis = api.notifications(since_id=_since_id)
        except:
            notis.append({'since_id':_since_id})
            return notis

        if len(_notis)<=0:
            break
            
        # pedimos las notificaciones
        for noti in _notis:
            try:
                if noti.id>_since_id:
                    _since_id = noti.id
                # pedimos el estado
                estado = api.status(noti.status.id)
                # lo limpiamos de simbolos
                soup = BeautifulSoup(estado.content,"lxml")
                notis.append({'texto':soup.get_text(),'estado':estado})
            except:
                # si da algun error pasamos a la siguiente iteraccion
                continue
                
    notis.append({'since_id':_since_id})
    return notis

def responder(texto, estado, api):
    try:
        return api.status_reply(estado, texto)
    except:
        return False

def lista_acciones(notis):
    acciones = []
    
    for noti in notis:
        palabras = noti['texto'].split()
        id = comando_pos(palabras)
        
        if id>=0:
            acciones.append({
                'accion':palabras[id],
                'parametros':palabras[id+1:],
                'estado':noti['estado']
            })
        
    return acciones


def comando_pos(palabras):
    '''
    -sid: devuelve el ultimo since_id
    -a: añade las urls pasadas
    -b: borra las urls pasadas
    -bu: borra todas las urls del usuario
    -e: convierte en enlace las urls pasadas
    -eu: convierte en enlace todas las urls del usuario
    -i: convierte en impulso las urls pasadas
    -iu: convierte en impulso todas las urls del usuario
    '''
    for com in ['sid','-a','-b','-bu','e','eu','-i','-iu']:
        try:
            return palabras.index(com)
        except:
            pass
    return -1

def sql_conectar(bd=':memory:'):
    try:
        con = sqlite3.connect(bd)
        #cur = con.cursor()
        return True,con
    except Error:
        return False,Error

def sql_crear_tabla(con):
    error,cur = sql_ejecutar(con,"CREATE TABLE IF NOT EXISTS hilos(urlHilo text, urlToot text, usuario text, publicado integer, borradoPorUsu integer)")
    error,cur = sql_ejecutar(con,"CREATE TABLE IF NOT EXISTS idNoti(since_id integer)")
    error,cur = sql_ejecutar(con,"INSERT INTO idNoti VALUES(0)")
    return error,cur

def sql_actualizar_since_id(con,since_id):
    error,cur = sql_ejecutar(con,'UPDATE idNoti SET since_id='+str(since_id))
    return error

def sql_pedir_since_id(con):
    error,cur = sql_ejecutar(con,'SELECT * FROM idNoti')
    dev = cur.fetchall()
    return dev[0][0]

def sql_insertar(con,urlHilo,urlToot,usu,publicado):
    error,cur = sql_ejecutar(con,"INSERT INTO hilos VALUES('"+str(urlHilo)+"','"+str(urlToot)+"','"+str(usu)+"', "+str(publicado)+" , 0)")
    return error

def sql_actualizar_hilo(con,urlHilo,urlToot,usu,publicado):
    error,cur = sql_ejecutar(con,'UPDATE hilos SET urlHilo="'+str(urlHilo)+'",urlToot="'+str(urlToot)+'",usuario="'+str(usu)+'",publicado="'+str(publicado)+'" where urlHilo="'+str(urlHilo)+'"')
    return error

def sql_resetear_publicados(con):
    error,cur = sql_ejecutar(con,'UPDATE hilos SET publicado=0 WHERE borradoPorUsu=0')
    return error

def sql_existe_hilo(con,urlHilo):
    error,cur = sql_ejecutar(con,'SELECT * FROM hilos where urlHilo="'+str(urlHilo)+'"')
    return cur.fetchall()
    
def sql_pedir_hilo(con,urlHilo,publicado):
    error,cur = sql_ejecutar(con,'SELECT * FROM hilos where urlHilo="'+str(urlHilo)+'" and publicado='+str(publicado))
    return cur.fetchall()

def sql_pedir_hilos(con,publicado):
    error, cur = sql_ejecutar(con,'SELECT * FROM hilos where publicado='+str(publicado)+' and borradoPorUsu=0')
    return cur.fetchall()

def sql_borrar_hilo(con,urlHilo):
    error,cur = sql_ejecutar(con,'DELETE hilos where urlHilo="'+str(urlHilo)+'" and borradoPorUsu=0')
    return error

def sql_borrar_hilo_por_usu(con,urlHilo):
    error,cur = sql_ejecutar(con,'UPDATE hilos SET borradoPorUsu=1 where urlHilo="'+str(urlHilo)+'"')
    return error

def sql_borrar_hilos(con,usu):
    error,cur = sql_ejecutar(con,'DELETE hilos where usuario="'+str(usu)+'" and borradoPorUsu=0')
    return error

def sql_borrar_hilos_por_usu(con,usu):
    error,cur = sql_ejecutar(con,'UPDATE hilos SET borradoPorUsu=1 where usuario="'+str(usu)+'"')
    return error

def sql_ejecutar(con,peticion):
    try:
        cur = con.cursor()
        cur.execute(peticion)
        con.commit()
        return [True,cur]
    except:
        return [False,False]
    
def sql_cerrar(con):
    con.close()

def estado_pedir_usuario(estado):
    usu = estado['account']['acct']
    if '@' in usu:
        pass
    else:
        usu = str(usu)+'@botsin.space'
    return usu

def accion_a(acc,api,con):
    urls = []
    urlsAgregadas = []
    urlsFallos = []
    urlsExisten = []
    for parametro in acc['parametros']:
        id,estado,msj = estado_existe(parametro,api)
        if id:
            urls.append([parametro,estado])
        else:
            urlsFallos.append(parametro)
            
    for url in urls:
        if not sql_existe_hilo(con,url[0]):
            usu = estado_pedir_usuario(url[1])
            '''
            usu = url[1]['account']['acct']
            if '@' in usu:
                pass
            else:
                usu = str(usu)+'@botsin.space'
           ''' 
            if sql_insertar(con,url[0],url[0],usu,0):
                urlsAgregadas.append(url[0])
            else:
                urlsFallos.append(url[0])
        else:
            urlsExisten.append(url[0])
    return [urlsAgregadas,urlsExisten,urlsFallos]
       
def accion_b(acc,usuEnvia,api,con):
    urls = []
    urlsBorradas = []
    urlsFallos = []
    urlsExisten = []
    for parametro in acc['parametros']:
        id,estado,msj = estado_existe(parametro,api)
        if id:
            urls.append([parametro,estado])
        else:
            urlsFallos.append(parametro)
            
    for url in urls:
        if sql_existe_hilo(con,url[0]):
            usu = estado_pedir_usuario(url[1])
            '''usu = url[1]['account']['acct']
            if '@' in usu:
                pass
            else:
                usu = str(usu)+'@botsin.space'
            '''
            if usu==usuEnvia:
                if sql_borrar_hilo_por_usu(con,url[0]):
                    urlsBorradas.append(url[0])
                else:
                    urlsFallos.append(url[0])
            elif CUENTA_ADMIN==usuEnvia:
                if sql_borrar_hilo(con,url[0]):
                    urlsBorradas.append(url[0])
                else:
                    urlsFallos.append(url[0])
            else:
                urlsFallos.append(url[0])
        else:
            urlsNoExisten.append(url[0])
    return [urlsBorradas,urlsNoExisten,urlsFallos]

def main(_since_id,con):
    masto = Mastodon(
        access_token = MASTO_TOKEN,
        api_base_url = MASTO_URL
    )

    # sacar el ultimo _since_id

    notis = lista_notis(_since_id, masto)
    
    if len(notis)==1:
        return _since_id
    
    for acc in lista_acciones(notis[:-1]):
        usuEnvia = acc['estado']['account']['acct']
        if '-a'==acc['accion']:
            if usuEnvia==CUENTA_ADMIN:
                urlsAgregadas,urlsExisten,urlsFallos = accion_a(acc,masto,con)
                mensaje = u'Añadidas: '+str(','.join(urlsAgregadas))
                mensaje = mensaje+u' Existen: '+str(','.join(urlsExisten))
                mensaje = mensaje+u' Fallos: '+str(','.join(urlsFallos))
                responder(mensaje, acc['estado'], masto)
            else:
                responder(u'No tienes autorización para añadir urls', acc['estado'], masto)
        elif '-b'==acc['accion']:
            urlsBorradas,urlsNoExisten,urlsFallos = accion_b(acc,usuEnvia,masto,con)
            mensaje = u'Borradas: '+str(','.join(urlsBorradas))
            mensaje = mensaje+u' No existen: '+str(','.join(urlsNoExisten))
            mensaje = mensaje+u' Fallos: '+str(','.join(urlsFallos))
            responder(mensaje, acc['estado'], masto)
        elif '-bu'==acc['accion']:
            accion_bu(acc,usuEnvia,masto,con)
        elif '-e'==acc['accion']:
            accion_e(acc,usuEnvia,masto,con)
        elif '-eu'==acc['accion']:
            accion_eu(acc,usuEnvia,masto,con)
        elif '-i'==acc['accion']:
            accion_i(acc,usuEnvia,masto,con)
        elif '-iu'==acc['accion']:
            accion_iu(acc,usuEnvia,masto,con)
        elif '-sid'==acc['accion'] and usuario==CUENTA_ADMIN:
            accion_sid(notis[-1]['since_id'], acc)
            
    # guardar el notis[-1]['since_id'] ultimo
    
    return notis[-1]['since_id']

''' 
error, con = sql_conectar()
sql_crear_tabla(con)
sql_insertar(con,'https://urlHilo.algo','https://urlToot.algo','@usu@dominio.algo',0)
sql_actualizar_hilo(con,'https://urlHilo.algo','https://urlToot2.algo','@usu@dominio2.algo',1)
hilos = sql_pedir_hilos(con,1)
for hilo in hilos:
    print hilo
sql_cerrar(con)
'''
error, con = sql_conectar()
sql_crear_tabla(con)
#id = sql_pedir_since_id(con)
id = 0
#main(id,con)

while True:
    id = main(id,con)
    sql_actualizar_since_id(con,id)
    hilos = sql_pedir_hilos(con,0)
    for hilo in hilos:
        print hilo
    time.sleep(10)

sql_cerrar(con)